"use strict"
let cart=JSON.parse(localStorage.getItem('cart'))||{};
function addToCart(item,qty,elem){
    item=String(item);
    
    qty=parseInt(qty)||0;
    if(qty<0){qty=0}
    //debugger
    cart[item]=parseInt(qty);
    localStorage.setItem('cart',JSON.stringify(cart));
    updateCartInput(item,elem);
    return false;
}
function incrementCart(item,elem){
    item=String(item);
    if(cart[item])cart[item]++
    else cart[item]=1;
    //debugger;
    localStorage.setItem('cart',JSON.stringify(cart));
    updateCartInput(item,elem);
    return false;
}
function decrementCart(item,elem){
    item=String(item);
    if(cart[item])cart[item]--;
    //debugger;
    localStorage.setItem('cart',JSON.stringify(cart));
    updateCartInput(item,elem);
    return false;
}
function updateCartInput(item,elem){
    item=String(item);
    if(window.setTotal){
        window.setTotal();
        if(window.setDiscount){
            window.setDiscount();
        }
    }
    if(!elem){
        return;
    }
    elem.value=cart[item];
}
let grads=['linear-gradient(315deg, #fc9842 0%, #fe5f75 74%)','linear-gradient(315deg, #fc5296 0%, #f67062 74%)','linear-gradient(315deg, #ff4e00 0%, #ec9f05 74%)','linear-gradient(315deg, #f06543 0%, #ffbe3d 74%)','linear-gradient(315deg, #20bf55 0%, #01baef 74%)','linear-gradient(315deg, #63a4ff 0%, #83eaf1 74%)','linear-gradient(315deg, #7f53ac 0%, #647dee 74%)',"linear-gradient(315deg, #42378f 0%, #f53844 74%)","linear-gradient(315deg, #726cf8 0%, #e975a8 74%)","linear-gradient(315deg, #bf033b 0%, #ffc719 74%)","linear-gradient(315deg, #08e1ae 0%, #98de5b 74%)","linear-gradient(315deg, #f39f86 0%, #f9d976 74%)","linear-gradient(315deg, #63a4ff 0%, #83eaf1 74%)","linear-gradient(315deg, #42378f 0%, #f53844 74%)","linear-gradient(315deg, #e6c84f 0%, #e8807f 74%)","linear-gradient(315deg, #7ee8fa 0%, #80ff72 74%)"];
let cols=['#007bff','#6610f2','#6f42c1','#e83e8c','#dc3545','#fd7e14','#ffc107','#28a745','#20c997','#17a2b8','#6c757d','#343a40','#007bff','#6c757d','#28a745'];
                    function randCol(){
                        return cols[Math.floor(Math.random()*(cols.length-1))]
                    }
                    function randGrad(){
                        return grads[Math.floor(Math.random()*(grads.length-1))]
                    }
                    document.addEventListener("DOMContentLoaded",function(){
                        document.body.style.background=randGrad();
                    })
let items=[
    {"id":"1","cat":"bk","name":"Classmate Soft Cover 6 Subject Spiral Binding Notebook, Single Line, 300 Pages","mrp":"160","price":"130","desc":"Classmate notebooks are made to the highest quality standards. Made from superior quality paper and pulp, the pages are whiter, brighter and smoother. The superior cut and excellent finish ensures that the pages are in perfect alignment without any folded corners. The superior binding strength and wrapper lamination make them as durable as ever. These notebooks come with a wide variety of covers with activities and trivias to make every classmate notebook unique in its own way.\nThe series comes in both lay flat and spiral binding with single subject and 6 subject options.","img":"1.jpg"},
    {"id":"2","cat":"bk","name":"Classmate Longbook (330 X 210) - 400 Pages - Unruled - Hard Cover","mrp":"185","price":"135","desc":"Classmate notebooks are made to the highest quality standards. Made from superior quality paper and pulp, the pages are whiter, brighter and smoother. The superior cut and excellent finish ensure the pages are in perfect alignment without any folded corners. The superior binding strength and wrapper lamination make them as durable as ever. These notebooks come with a wide variety of covers with activities and trivias to make every classmate notebook unique in its own way.","img":"2.jpg"},
    {"id":"3","cat":"bk","name":"Classmate Drawing Book - Soft Cover, 40 Pages, 275x347mm, Unruled","mrp":"540","price":"440","desc":"ITC Classmate Notebooks are the best study needs to be used for educational purposes or best to be served as a stationery needs. The Classmate range of products is targeted at satisfying education and stationery needs of students and young adults .","img":"3.jpg"},
    {"id":"4","cat":"bk","name":"Classmate Notebook - Ruled, Small, Single Line, Hard Bound, 172 Pages","mrp":"100","price":"50","desc":"Classmate Notebooks are made to the highest quality standards. Made from superior quality paper and pulp, the pages are whiter, brighter and smoother. The superior cut and excellent finish ensure the pages are in perfect alignment without any folded corners. The superior binding strength and wrapper lamination make them as durable as ever. These notebooks come with a wide variety of covers with activities and trivias to make every classmate notebook unique in its own way.","img":"4.webp"},
    {"id":"5","cat":"cw","name":"Paraspapermart A4 Handmade Paper (Assorted Colour, 10 Color X 1 Sheet Each) - Pack of 10 Sheets","mrp":"300","price":"100","desc":"A4 pink textured craft paper/mount board for art and craft work. Comes in pack of 2 board 2mm thick.","img":"5.jpg"},
    {"id":"6","cat":"cw","name":"Colourful Seed Cut Glass Beads","mrp":"300","price":"100","desc":"Colourful beads to enhance the beauty of your craftwork.","img":"6.jpeg"},
    {"id":"7","cat":"cw","name":"16 Colors 800 Yard Satin Ribbons, baotongle Fabric Ribbon Silk Satin Roll Satin Ribbon Rolls","mrp":"300","price":"250","desc":"You can use it for places:\n\n- Decorative gift box\n- DIY candy box accessories\n- Decorative new scarf\n- Hair band wind chimes\n- Sweater chain\n- Pattern bow tie\n- Decorative pouch\n- Decorative side of clothes\n- Embroidered cushions (three-dimensional effect is good for decoration)\n- Decorative painting handmade dolls\n- Combs, gift wrapping, wedding room layout or birthday party decoration: If you put more ribbons on the balloons, the effect is really cool.","img":"7.webp"},
    {"id":"8","cat":"cw","name":"Full Chart Paper Size ( 70 x 56 cm ),Premium Quality Both Side Colored Multi Use Pastel Craft Paper, 300 GSM ,10 Vibrant Colors","mrp":"890","price":"300","desc":"Both side Colored Pastel Multi use craft paper in 10 eye catching and most used colors. Full Chart Paper Size ( 70 x 56 cm ) 300 GSM,10 Vibrant Colors Sheets are both side colored and are machine cut neatly and perfectly Sheets do not break when folded. Completely recyclable and safe for children to use. Appropriate for card making, creating paper products, students school projects and various art and craft activities. Use them in Scrap booking, Card making, Paper crafts, School projects, Kids craft, chart making & general craft. Also suitable for festive decoration, stamping, signage making and die-cutting.","img":"8.jpeg"},
    {"id":"9","cat":"es","name":"Apsara Non Dust Eraser, 20 pcs","mrp":"60","price":"40","desc":"Soft and smooth. Erases Graphite gently without damaging paper.","img":"9.jpg"},
    {"id":"10","cat":"es","name":"Apsara Long Point Sharpeners - Pack of 20","mrp":"100","price":"50","desc":"Scientifically designed blade for elegant long tip\nLong tip writes more\nMeets international quality standards","img":"10.jpg"},
    {"id":"11","cat":"es","name":"DOMS PENCIL SHARPENER 1 PCS","mrp":"5","price":"1","desc":"Doms Pencil Sharpener\nContoured body for a grip\nDurable plastic body\nSharpens up to 7 pencils\nColour: Red, Blue, Green, Yellow and Black body colours. \nColour of the deliver product is subject to stock availability. \nSingle Hole for Standard (8.0mm) pencil \nSmooth & Effortless Sharpening.","img":"11.jpg"},
    {"id":"12","cat":"es","name":"Rubber White Nataraj Eraser","mrp":"5","price":"1","desc":"No description provided.","img":"12.jpg"},
    {"id":"13","cat":"es","name":"Nataraj 621 Sharpener","mrp":"3","price":"2","desc":"Specially angled blade for easy sharpening\nAntirust coating for long lasting blade edge\nContoured body for firm grip\nAvailable in 5 attractive colours","img":"13.jpg"},
    {"id":"14","cat":"es","name":"Nataraj 621 Sharpener (20 pcs)","mrp":"60","price":"40","desc":"Specially angled blade for easy sharpening\nAntirust coating for long lasting blade edge\nContoured body for firm grip\nAvailable in 5 attractive colours","img":"14.jpg"},
    {"id":"15","cat":"es","name":"Apsara Non Dust Jumbo Eraser","mrp":"5","price":"4","desc":"Soft and smooth\nErases Graphite gently without damaging paper\nPicks up dust while erasing\nIdeal for students, artists, engineers and architects","img":"15.jpg"},
    {"id":"16","cat":"misc","name":"Camlin Kokuyo Office Highlighter - Pack of 5 Assorted Colors","mrp":"100","price":"90","desc":"Chisel tip for underlining and Highlighting\nWater based fluorescent ink for better visibility\nAvailable 5 Fluorescent colours ( Yellow, Pink, Orange, Green, Blue)\nNon-Toxic","img":"16.webp"},
    {"id":"17","cat":"misc","name":"KESETKO Paper Clips, U Clips, Gem Clips 30mm, (200 Clips) Multicolored Packed in One Box, for Office, Home","mrp":"240","price":"230","desc":"Paper U Clips, Paper Gem Clips 30mm,\n200 Pcs in 1 Plastic Box and this box is reusable.\nU Shape Clips Hold Documents Firmly, High Quality Superior Grip, non-tear ends and reusable. Packed in transparent storage box.","img":"17.webp"},
    {"id":"18","cat":"misc","name":"Fevicol MR Squeeze Bottle, 200 grams","mrp":"75","price":"70","desc":"Bonds paper, cardboard, thermocol, fabrics, wood, plywood, etc.\nStrong bonding.\nReady to use and non-staining.","img":"18.jpg"},
    {"id":"19","cat":"misc","name":"Large Multifunctional Black Binder Clip","mrp":"10","price":"5","desc":"Size: 5 CM(1.96 inch).\nRoll into the slot-shaped design, without damaging the paper, clip more securely.\nIdeal for reuse, clips spring back into shape when removed.\nHandles can fold up for handing or can be removed for binding.\nFor a variety of documents, notes, data, pictures, papers classified management.","img":"19.jpg"},
    {"id":"20","cat":"misc","name":"3M Post-It Notes, Original Pads, 3 X 3 Inches, 100 Sheets per Pad","mrp":"65","price":"60","desc":"Post-it Notes are the perfect size for notes, numbers and lists.\nRepositionable adhesive won't mark paper and other surfaces.\nAttach notes without staples, paper clips or tape.\nUse the self-adhesive top strip to stick them on papers, desks and other places where you need a reminder.\nPaper used comes from well-managed forests where trees are replanted.","img":"20.jpg"},
    {"id":"21","cat":"pp","name":"Parker Jotter Standard CT Ball Pen (Blue)","mrp":"210","price":"180","desc":"Finish: Gloss.\nBody Material: ABS Plastic and Stainless Steel.\nNib Feature: Tungsten Carbide Ball.\nInk Colour: Blue.","img":"21.jpg"},
    {"id":"22","cat":"pp","name":"Classmate Octane Gel Pen- Neon Series (Blue)- Pack of 10 Pens + 1 Pen FREE","mrp":"100","price":"80","desc":"5 vibrant Neon body colours.\nSmooth and fast writing.\nJapanese waterproof ink.","img":"22.jpg"},
    {"id":"23","cat":"pp","name":"Pilot V7 Hi-tecpoint Roller ball pen with Cartridge System - 2 Blue Pens, 4 cartridges","mrp":"135","price":"90","desc":"For the first time! Pilot V7 Tecpoint Pen comes with rechargeable cartridge system.\n4 V7 cartridges with this special pack.\nIdeal for school and college students.\nWrites twice as much as a gel pen.\nPure liquid ink for smooth Skip free writing.","img":"23.jpg"},
    {},{"id":"25","cat":"pp","name":"Nataraj Half Size Colour Pencil - 10 Shades","mrp":"90","price":"80","desc":"Brilliant colours with good colour mixing quality.\nStrong lead resists breaking.\nAttractive packaging.","img":"25.jpg"},
    {"id":"26","cat":"pp","name":"Apsara Platinum Extra Dark Pencils - Pack of 10","mrp":"50","price":"45","desc":"Specially designed for executives, 2B Grade.\nSuperior bonded lead is strong and resists breakage.\nSuperior quality wood makes sharpening easy.\nMeets international quality standards.","img":"26.jpg"},
    {"id":"27","cat":"pp","name":"Apsara Absolute Extra Dark Pencils - Pack of 10","mrp":"70","price":"65","desc":"50 % stronger lead that resists breaking while writing and sharpening.\nSpecial wood for easy sharpening.\nExtra dark lead for good handwriting.","img":"27.jpg"},
    {},{"id":"29","cat":"pp","name":"APSARA MATT MAGIC EXTRA DARK PENCIL","mrp":"250","price":"240","desc":"50 APSARA MATT MAGIC EXTRA DARK PENCILS","img":"29.jpg"},
    {"id":"30","cat":"pp","name":"Faber-Castell Triangular Colour Pencils - Pack of 12 (Assorted)","mrp":"200","price":"199","desc":"Best of class color pencils with smooth color rich leads and special bonding that offers good sharpen ability and high break resistance.\nPhthalate free lacquer on our pencils makes it safe for children.\nSoft lead-no pressure required for coloring.\nRich, smooth and easy spread provides good coverage.","img":"30.jpg"},
    {"id":"31","cat":"pp","name":"Ball Point Plastic Cello Gripper Ball Pen","mrp":"7","price":"5","desc":"No description provided.","img":"31.jpg"},
    {"id":"32","cat":"pp","name":"Cello Butterflow Ball Point Pen","mrp":"10","price":"5","desc":"No description provided.","img":"32.jpg"},
    {"id":"33","cat":"pp","name":"Cello Butterflow Color Pen Set - Pack of 10 (Multicolor)","mrp":"150","price":"149","desc":"5 fashion ink colors.\nBold 1mm tip for easy writing or coloring.\nSmoothest writing with butterflow ink.","img":"33.jpg"},
    {"id":"34","cat":"pp","name":"Nataraj 621 Writing Pencils ( Pack Of 10 )","mrp":"40","price":"30","desc":"HB, Bonded Lead Pencils, Smooth Writing.\nFree Sharpner & Eraser.\nWith Every 10 Pencil Pack.","img":"34.jpg"},
    {"id":"35","cat":"sg","name":"Camlin Kokuyo Scholar Mathematical Drawing Instruments","mrp":"100","price":"99","desc":"Specially designed self centering compass, for ease and accuracy while drawing circles and angles.\nBoth divider and compass are made with non-rusting strong material to last long and remain in shape and shine.\nThe plastic used in ruler, protractor and set square are made of high transparency plastic and comes with precise marking for easy reading and accurate drawings.\nThe special technique of marking ensures that the marking are clearly visible after extensive use.\nProduct comes with a free 0.7mm Camlin Scholar Pen Pencil.","img":"35.jpg"},
    {"id":"36","cat":"sg","name":"Camlin Kokuyo Plastic Scale, 30cm","mrp":"12","price":"10","desc":"Highly Transparent plastic scale with precise marking for easy reading and accurate drawings.\nSmooth tapered edges that lie flat on the surface to give sharp lines.\nRound edges that do not chip.\nClear and distinctly visible markings.","img":"36.jpg"},
    {"id":"37","cat":"sg","name":"Camlin Kokuyo Exam Compass","mrp":"25","price":"20","desc":"The compass is made with non-rusting strong material.\nThis specially designed self centering compass allows ease and accuracy while drawing circles and angles.\nLast long and remain in shape and shine.","img":"37.jpg"},
    {"id":"38","cat":"sg","name":"Camlin Kokuyo Exam Standard Scale - 15cm","mrp":"5","price":"4","desc":"Highly Transparent plastic scale with precise marking for easy reading and accurate drawings.\nSmooth tapered edges that lie flat on the surface to give sharp lines.\nRound edges that do not chip.\nClear and distinctly visible markings.","img":"38.jpg"},
    {"id":"39","cat":"wb","name":"Faber-Castell Synthetic Round Hair Paint Brush (Assorted Set of 7)","mrp":"250","price":"249","desc":"For Acrylic and Oil Colours.\nAvailable with Round and Flat brushes.","img":"39.jpeg"},
    {"id":"40","cat":"wb","name":"Camel Student Poster Color - 10ml each, 12 Shades","mrp":"170","price":"149","desc":"12 assorted shades of 10 ml each.\nConfirms to safety standard EN 71 - 3.\nCamel art contest entry coupon inside the pack.\nBest result can be seen on drawing paper.\nAttractive and Durable tin packing.","img":"40.jpg"},
    {"id":"41","cat":"wb","name":"Camlin Kokuyo Water Colour Cakes","mrp":"225","price":"200","desc":"18 assorted shades.\nMade from rich, long lasting pigments.\nConforms to child safety standards.","img":"41.jpg"}
    ];
